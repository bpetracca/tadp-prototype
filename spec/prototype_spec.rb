require 'rspec'
require_relative '../src/prototype'

describe 'Testing prototype' do

  before do
    @guerrero = PrototypeObject.new
    @guerrero.set_property(:energia, 100)
    @guerrero.set_property(:potencial_defensivo, 10)
    @guerrero.set_property(:potencial_ofensivo, 30)

    @guerrero.set_method(:atacar_a,
                        proc do |otro_guerrero|
                          if otro_guerrero.potencial_defensivo < self.potencial_ofensivo
                            otro_guerrero.recibe_danio(self.potencial_ofensivo - otro_guerrero.potencial_defensivo)
                          end
                        end)
    @guerrero.set_method(:recibe_danio, proc do |danio| self.energia-=danio end)
    @otro_guerrero = @guerrero.clone #clone es un metodo que ya viene definido en Ruby

  end

  it 'set_property setea valores' do

    @guerrero = PrototypeObject.new
    @guerrero.set_property :energia, 100

    expect(@guerrero.energia).to eq(100)
  end

  it 'set_method setea metodos' do

    @guerrero = PrototypeObject.new
    @guerrero.set_method :retornar_hola, proc { 'hola' }

    expect(@guerrero.retornar_hola).to eq('hola')
  end

  it 'copiado del enunciado' do
    @guerrero.atacar_a @otro_guerrero

    expect(@otro_guerrero.energia).to eq(80)
  end

  it 'agrego espadachin' do
    espadachin = PrototypeObject.new

    espadachin.set_prototype(@guerrero)
    espadachin.set_property(:habilidad, 0.5)
    espadachin.set_property(:potencial_espada, 30)
    espadachin.set_property(:potencial_ofensivo, 30) #evaluar si conviene (o no) copiar el estado
    espadachin.energia = 100

    espadachin.set_method(:potencial_ofensivo, proc {
      @potencial_ofensivo + self.potencial_espada * self.habilidad
    })

    espadachin.atacar_a(@guerrero)

    expect(@guerrero.energia).to eq(65)
  end
end